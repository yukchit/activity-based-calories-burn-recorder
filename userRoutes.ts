// ts by CHiT

import express, { Request, Response } from 'express';
import { User } from './models';
import { client, upload } from './main';
import { checkPassword, hashPassword } from './hash';
import { isLoggedInApi } from './guards';
import { logger } from './logger';
import fetch from 'node-fetch';


// ts by CHiT
export const userRoutes = express.Router();
userRoutes.post('/login', login);
userRoutes.get('/login/google', loginGoogle);
userRoutes.get('/logout', logout);
userRoutes.post('/createUser', upload.single('profilePic'), createUser);
userRoutes.get('/loggedInMainPage', isLoggedInApi, loggedInIndex);

// ts by wung
userRoutes.post('/editProfile', isLoggedInApi, edit);


// ts by CHiT
async function login(req: Request, res: Response) {
    try {
        const { email, password } = req.body;
        const result = await client.query(/* sql */ `select * from users where email = $1`, [email]);
        const users: User[] = result.rows;
        const user = users[0];
        if (user && await checkPassword(password, user.password)) {
            // login 成功
            if (req.session) {
                req.session.user = user;   // 係個session度mark咗你係成功login咗
            }
            res.redirect('/StartRecord.html');
            return;
        } else {
            // login 失敗
            res.status(401).redirect('/?error=login+failed');
        }
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[USER001] internal server error' });
    }
};


async function loginGoogle(req: express.Request, res: express.Response) {
    try {
        const accessToken = req.session?.grant.response.access_token;

        // res.json({access_token: accessToken});
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            method: "get",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        });
        const result = await fetchRes.json();
        const users = (await client.query(/* sql */`SELECT * FROM users WHERE email = $1`, [result.email])).rows;
        let user = users[0];
        if (!user) {
            // 開返個新account
            // user = (await client.query(/* sql */`insert into users (email, password) values ($1, $2) returning *`, [result.email, await hashPassword(Math.random().toString(36).substring(2))])).rows[0];

            // return res.redirect('/createaccount.html')
            return res.redirect(`/createaccount.html?SignUpMsg=Please+Fill+In+More+Information+With+Your+Google+Email&SignUpEmail=${result.email}&SignUpName=${result.name}`);
        }
        if (req.session) {
            req.session.user = user;
            console.log(req.session.user);
        }
        return res.redirect('/StartRecord.html');
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[USER002] internal server error' });
    }
}



// ts by wung 
//adding options of user's favourites & uploading profile pic by CHiT
async function createUser(req: Request, res: Response) {
    try {
        const { userName, userPassword, userEmail, userGender, userHeight, userWeight, userBirthday, userFavourites } = req.body;
        const birthday = new Date(userBirthday);
        const filename = req.body.profilePic != 'undefined' ? req.file.filename : null;
        await client.query(/* sql */`INSERT INTO users (name, password, email, gender, height, weight, date_of_birth, favourites, total_calories_burned, profile_pic, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, NOW(), NOW()) RETURNING id`
            , [userName, await hashPassword(userPassword), userEmail, userGender, userHeight, userWeight, birthday, userFavourites, 0, filename]);

        // if (req.body.profilePic != 'undefined') {
        //     const { userName, userPassword, userEmail, userGender, userHeight, userWeight, userBirthday, userFavourites } = req.body;
        //     const birthday = new Date(userBirthday)
        //     await client.query(/* sql */`INSERT INTO users (name, password, email, gender, height, weight, date_of_birth, favourites, total_calories_burned, profile_pic, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, NOW(), NOW()) RETURNING id`
        //         , [userName, await hashPassword(userPassword), userEmail, userGender, userHeight, userWeight, birthday, userFavourites, 0, req.file.filename]);
        // } else {
        //     const { userName, userPassword, userEmail, userGender, userHeight, userWeight, userBirthday, userFavourites } = req.body;
        //     const birthday = new Date(userBirthday)
        //     await client.query(/* sql */`INSERT INTO users (name, password, email, gender, height, weight, date_of_birth, favourites, total_calories_burned, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW(), NOW()) RETURNING id`
        //         , [userName, await hashPassword(userPassword), userEmail, userGender, userHeight, userWeight, birthday, userFavourites, 0]);
        // }

        console.log("acc created")
        res.json({ success: true });
        // res.redirect('/?accSuccess=Congratulations!+Account+Created!'); 
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[USER003] internal server error' });
    }

}



// ts by CHiT
async function logout(req: Request, res: Response) {
    try {
        if (req.session) {
            console.log(req.session.user);
            delete req.session.user;
        }
        res.redirect('/');
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[USER004] internal server error' });
    }

}



// ts by wung

export async function edit(req: Request, res: Response) {
    try {
        const userID = req.session?.user.id;
        // let userID: Number;
        // if (req.session) {
        //     userID = req.session.user.id
        // }

        const { name, height, weight } = req.body
        client.query(/*sql*/`UPDATE users SET name = $1 where id = $2`, [name, userID]);
        client.query(/*sql*/`UPDATE users SET height = $1 where id = $2`, [height, userID]);
        client.query(/*sql*/`UPDATE users SET weight = $1 where id = $2`, [weight, userID]);
        res.redirect('/profile.html');
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[USER005] internal server error' });
    }
}


// ts by CHiT
async function loggedInIndex(req: Request, res: Response) {
    try {
        const userID = req.session?.user.id;
        // let userID: Number;
        // if (req.session) {
        //     userID = req.session.user.id
        // }

        const user: User = (await client.query(/*sql*/`select name from users where id = $1`, [userID])).rows[0];
        res.json({ name: user.name });
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[USER006] internal server error' });
    }
}