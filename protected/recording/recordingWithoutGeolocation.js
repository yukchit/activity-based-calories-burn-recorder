// js by CHiT


// Timer

var myVar = setInterval(start, 1000);
var Seconds = 0;

function resumeTimer() {
    myVar = setInterval(start, 1000);
    document.querySelector('#submit-button-div')
        .classList.add('d-none');
    document.querySelector('#resume-button')
        .classList.add('d-none');
    document.querySelector('#pause-button')
        .classList.remove('d-none');
}

function start() {
    var d = new Date();
    document.getElementById("realTime").innerHTML = d;
    Seconds++;
    document.getElementById("secondsPassed").innerHTML = Seconds;
}

function stopTimer() {
    clearInterval(myVar);
    document.querySelector('#submit-button-div')
        .classList.remove('d-none');
    document.querySelector('#resume-button')
        .classList.remove('d-none');
    document.querySelector('#pause-button')
        .classList.add('d-none');
}


// AJAX

document.querySelector('#timer-form')
    .addEventListener('submit', async (event) => {
        event.preventDefault();   // No need to reload the page
        const form = event.target.children
        const formObject = {
            activity: form.activity.innerText,
            secondsPassed: parseFloat(form.secondsPassed.innerText),
            endingTime: Date.parse(form.endingTimeTimer.innerText),
            activityName: form[6].children[0].value,
            distanceTravelled: null,
            speedCalculated: null,
        }

        // missing headers Content-Type: 'application/json'
        const res = await fetch('/records', {
            headers: {
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(formObject)        /*JSON.stringify */
        });        //your_hostname/records


        window.location = ('/Activities.html')

    })

