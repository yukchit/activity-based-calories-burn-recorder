// js by CHiT

//Get distance travelled from geolocation

//Using Haversine Formula

var totalDistance = 0.0;
var lastLat;
var lastLong;

Number.prototype.toRadians = function () {
    return this * Math.PI / 180;
}

function distance(latitude1, longitude1, latitude2, longitude2) {
    // R is the radius of the earth in kilometers
    var R = 6371;

    var deltaLatitude = (latitude2 - latitude1).toRadians();
    var deltaLongitude = (longitude2 - longitude1).toRadians();
    latitude1 = latitude1.toRadians(), latitude2 = latitude2.toRadians();
    // longitude1 = longitude1.toRadians(), longitude2 = longitude2.toRadians();

    var a = Math.sin(deltaLatitude / 2) * Math.sin(deltaLatitude / 2) + Math.cos(latitude1) * Math.cos(latitude2) * Math.sin(deltaLongitude / 2) * Math.sin(deltaLongitude / 2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    var d = R * c;
    return d;
}

function updateErrorStatus(message) {
    document.getElementById("status").style.background = "papayaWhip";
    document.getElementById("status").innerHTML = "<strong>Error</strong>: " + message;
}

function updateStatus(message) {
    document.getElementById("status").style.background = "paleGreen";
    document.getElementById("status").innerHTML = message;
}

function loadDemo() {
    if (navigator.geolocation) {
        document.getElementById("status").innerHTML = "HTML5 Geolocation is supported in your browser.";
        navigator.geolocation.watchPosition(updateLocation, handleLocationError, { enableHighAccuracy: true, maximumAge: 0, timeout: 15000 });
    }
}

function updateLocation(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    var accuracy = position.coords.accuracy;
    var timestamp = position.timestamp;

    document.getElementById("latitude").innerHTML = "Latitude: " + latitude;
    document.getElementById("longitude").innerHTML = "Longitude: " + longitude;
    document.getElementById("accuracy").innerHTML = "Accuracy: " + accuracy + " meters";
    document.getElementById("timestamp").innerHTML = "Timestamp: " + new Date(timestamp);

    // sanity test... do not calculate distance if accurancy value too large
    if (accuracy >= 30000) {
        updateStatus("Need more accurate values to calculate distance.");
        return;
    }

    // calculate distance
    if ((lastLat != null) && (lastLong != null)) {
        var currentDistance = distance(latitude, longitude, lastLat, lastLong);

        document.getElementById("currDist").innerHTML =
            "Current Distance Traveled: " + currentDistance.toFixed(2) + " km";

        totalDistance += currentDistance;
        document.getElementById("totalDist").innerHTML =
            currentDistance.toFixed(2);
        updateStatus("Location successfully updated.");
    }
    lastLat = latitude;
    lastLong = longitude;
}

function handleLocationError(error) {
    switch (error.code) {
        case 0:
            updateErrorStatus("There was an error while retrieving your location. Additional details: " + error.message);
            break;
        case 1:
            updateErrorStatus("The user opted not to share his or her location.");
            break;
        case 2:
            updateErrorStatus("The browser was unable to determine you location. Additional details: " + error.message);
            break;
        case 3:
            updateErrorStatus("The browser timed out before retrieving the location.");
            break;
    }
}



// Timer starts when (navigator.geolocation) is true

var myVar = setInterval(start, 1000);
var Seconds = 0;

function resumeTimer() {
    myVar = setInterval(start, 1000);
    document.querySelector('#submit-button-div')
        .classList.add('d-none');
    document.querySelector('#resume-button')
        .classList.add('d-none');
    document.querySelector('#pause-button')
        .classList.remove('d-none');
}

function start() {
    if (navigator.geolocation) {
        var d = new Date();
        document.getElementById("realTime").innerHTML = d;
        Seconds++;
        document.getElementById("secondsPassed").innerHTML = Seconds;
    }
}

function stopTimer() {
    clearInterval(myVar);
    document.querySelector('#submit-button-div')
        .classList.remove('d-none');
    document.querySelector('#resume-button')
        .classList.remove('d-none');
    document.querySelector('#pause-button')
        .classList.add('d-none');
}





// AJAX by CHiT

document.querySelector('#timer-form')
    .addEventListener('submit', async (event) => {
        event.preventDefault();   // No need to reload the page
        const form = event.target;
        const totalDist = form.children[1].children[0].innerText;


        async function findingIntensityClass() {
            let intensityClass = "";
            if (totalDist) {
                if (form.children.activity.innerText == "Walking" || form.children.activity.innerText == "Running") {
                    walkOrRunSpeed = totalDist / (form.children[4].innerText / 60 / 60);
                    if (walkOrRunSpeed <= 4) {
                        intensityClass = "walk";
                    } else if (walkOrRunSpeed > 4 && walkOrRunSpeed <= 4.83) {
                        intensityClass = "walkingModerate";
                    } else if (walkOrRunSpeed > 4.83 && walkOrRunSpeed <= 6.44) {
                        intensityClass = "walkingVeryBriskPace";
                    } else if (walkOrRunSpeed > 6.44 && walkOrRunSpeed <= 7.9) {
                        intensityClass = "runningGeneral";
                    } else if (walkOrRunSpeed > 7.9 && walkOrRunSpeed <= 8) {
                        intensityClass = "running8";
                    } else if (walkOrRunSpeed > 8 && walkOrRunSpeed <= 8.37) {
                        intensityClass = "running837";
                    } else if (walkOrRunSpeed > 8.37 && walkOrRunSpeed <= 9.66) {
                        intensityClass = "running966";
                    } else if (walkOrRunSpeed > 9.66 && walkOrRunSpeed <= 10.78) {
                        intensityClass = "running1078";
                    } else if (walkOrRunSpeed > 10.78 && walkOrRunSpeed <= 11.26) {
                        intensityClass = "running1126";
                    } else if (walkOrRunSpeed > 11.26 && walkOrRunSpeed <= 12) {
                        intensityClass = "running12";
                    } else if (walkOrRunSpeed > 12 && walkOrRunSpeed <= 12.85) {
                        intensityClass = "running1285";
                    } else if (walkOrRunSpeed > 12.85 && walkOrRunSpeed <= 13.84) {
                        intensityClass = "running1384";
                    } else if (walkOrRunSpeed > 13.84 && walkOrRunSpeed <= 14.48) {
                        intensityClass = "running1448";
                    } else if (walkOrRunSpeed > 14.48 && walkOrRunSpeed <= 16.09) {
                        intensityClass = "running1609";
                    } else if (walkOrRunSpeed > 16.09) {
                        intensityClass = "running1754";
                    }
                } else if (form.children.activity.innerText == "Hiking") {
                    intensityClass = "hiking"
                } else if (form.children.activity.innerText == "Cycling") {
                    cyclingSpeed = totalDist / (form.children[4].innerText / 60 / 60)
                    if (cyclingSpeed <= 16.09) {
                        intensityClass = "cyclingLeisure";
                    } else if (cyclingSpeed > 16.09 && cyclingSpeed <= 19.15) {
                        intensityClass = "cycling1915";
                    } else if (cyclingSpeed > 19.15 && cyclingSpeed <= 22.37) {
                        intensityClass = "cyclingGeneral";
                    } else if (cyclingSpeed > 22.37 && cyclingSpeed <= 25.59) {
                        intensityClass = "cycling2559";
                    } else if (cyclingSpeed > 25.59 && cyclingSpeed <= 32.19) {
                        intensityClass = "cycling3058";
                    } else if (cyclingSpeed > 32.19) {
                        intensityClass = "cycling3219";
                    }
                }
            } else {
                intensityClass = "errorNoDistanceRecorded";
            }
            return intensityClass;
        }

        const speedFromDistanceTravelled = totalDist / (form.children[4].innerText / 60 / 60)


        const formObject = {
            activity: await findingIntensityClass(),
            secondsPassed: form.children[4].innerText,
            endingTime: Date.parse(form.children.endingTimeTimer.innerText),
            activityName: form.children[8].children[0].value,
            distanceTravelled: totalDist? totalDist: 0,
            speedCalculated: speedFromDistanceTravelled,
        }


        // missing headers Content-Type: 'application/json'
        const res = await fetch('/records', {
            headers: {
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(formObject)        /*JSON.stringify */
        });        //your_hostname/records

        console.log(formObject);
        window.location = ('/Activities.html');

    })