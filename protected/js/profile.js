// AJAX by Samson

async function loadProfile() {
    try {
        const res = await fetch("/myProfileDetail")
        const details = await res.json();



        function edit() {
            document.querySelector('#editButton').addEventListener('click', function (event) {
                console.log(details[0])
                document.getElementById('containerBox').innerHTML = ""
                document.getElementById('containerBox').innerHTML += `
        <form action="editProfile" method="POST">
        ${
                    details[0].profile_pic ? `
        <div class="container-fluid bodyBox">
            <div class="profileBox">
                <div class="profilePicBox">
                    <img src="/uploads/${details[0].profile_pic}" class="rounded d-block" width="200px" alt="personal profile picture">
                </div>
            </div>
        </div>
        `
                        : `
                
        <div class="container-fluid bodyBox">
            <div class="profileBox">
                <div class="profilePicBox">
                    <img src="/icons/blank-profile-picture.png" class="img-fluid" 
                    alt="blank profile picture">
                </div>
            </div>
        </div>`
                    }
            <table class="nameBox table">
                <thead class="">
                    <tr>
                        <th>
                            Name:<input type="text" class="nameBox black-border" name="name" required>
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Email Address: ${details[0].email}
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Gender: ${details[0].gender}
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Height:<input type="number" class="nameBox black-border" name="height" max="300" required> cm
                        </th>
                    </tr>
                    <tr>
                        <th>
                            Weight:<input type="number" class="nameBox black-border" name="weight" max="300" required> kg
                        </th>
                    </tr>
                    ${details[0].favourites ?
                        `
                        <tr>
                            <th scope="row">Favourites: ${details[0].favourites}</th>
                        </tr>
                        `:
                        `
                        <tr>
                            <th scope="row">Favourites: </th>
                        </tr>
                        `
                    }
                    <tr>
                        <th scope="row"> Total Calories burned: ${details[0].total_calories_burned} kcal</th>
                    </tr>
                </thead>
            </table>
    <input class="btn btn-success" type="submit" value="Submit">
    <tr class="cancelButtonBox">
        <a href="/profile.html" class="btn btn-success">Cancel</a>
    </tr> 
    </br>
    </br>
    </form>
    `
            })
        }



        const profileContainer = document.getElementById('containerBox')
        profileContainer.innerHTML = "";
        for (let detail of details) {
            profileContainer.innerHTML += `
        ${
                detail.profile_pic ? `
        <div class="nameBox">
            <img src="/uploads/${detail.profile_pic}" class="rounded d-block profile-pic-img" width="200px">
        </div>`
                    : `
                

                <div class="profilePicBox">
                    <img src="/icons/blank-profile-picture.png" class="img-fluid profile-pic-img" 
                    alt="blank profile picture">
                </div>

        `
                }
        
        <table class="nameBox table">
            <thead class="">
                <tr>
                    <th scope="row">Name: ${detail.name}</th>
                </tr>

                <tr>
                    <th scope="row">Email Address: ${detail.email}</th>
                </tr>

                <tr>
                    <th scope="row">Gender: ${detail.gender}</th>
                </tr>

                <tr>
                    <th scope="row">Height: ${detail.height}cm</th>
                </tr>

                <tr>
                    <th scope="row"> Weight: ${detail.weight}kg</th>
                </tr>

                ${detail.favourites ?
                    `
                <tr>
                    <th scope="row">Favourites: ${detail.favourites}</th>
                </tr>
                `:
                    `
                <tr>
                    <th scope="row">Favourites: </th>
                </tr>
                `
                }
                    <tr>
                      <th scope="row"> Total Calories burned: ${detail.total_calories_burned} kcal</th>
                    </tr>
                   
            </thead>
            
        </table>
    <tr class="editButtonPosition">
        <button class="btn btn-success" id="editButton">Edit</button>
    </tr> 
    </br>
    </br>
        `
            edit()
        }
    } catch (e) {
        console.error(e.message);
        res.status(500).json({ message: 'internal server error' });
    }
}

loadProfile()




// function edit () {  document.querySelector('#editButton').addEventListener('click', function(event){
//     document.getElementById('#containerBox').innerHTML += `
//             <form action="editProfile" method="POST">
//             <div class="nameBox">
//                 <img src="/uploads/${detail.profile_pic}" class="rounded d-block" width="200px">
//             </div>
//             <label>Name:
//                 <input type="text" class="nameBox black-border" name="name">
//             </label>
//             <div class="nameBox black-border">
//                 Email Address: ${detail.email}
//             </div>
//             <div class="nameBox black-border">
//                 Gender: ${detail.gender}
//             </div>
//             <label>Height:
//                 <input type="number" class="nameBox black-border" name="height">
//             </label>
//             <label>Weight:
//                 <input type="number" class="nameBox black-border" name="weight">
//             </label>
//     </div>
//     ${detail.favourites ?
//     `<div class="nameBox black-border container">Favourites: ${detail.favourites}</div>
//     `:
//     `
//     <div class="nameBox black-border container">
//         Favourites:
//     </div>
//     `
//     }
//     ${
//     detail.total_calories_burned ?
//     `
//     <div class="nameBox black-border container">
//         Total Calories burned: ${detail.total_calories_burned}
//     </div>
//     `

//     :`
//     <div class="nameBox black-border container">
//         Total Calories burned: ${detail.total_calories_burned}
//     </div>
//     `
//     }

//     <input type="submit" value="submit">
//     </form>
//     `
// })
// }

// `
//         <form action="editProfile" method="POST">
//         <div class="nameBox">
//             <img src="/uploads/${detail.profile_pic}" class="rounded d-block" width="200px">
//         </div>
//         <label>Name:
//             <input type="text" class="nameBox black-border" name="name">
//         </label>
//         <div class="nameBox black-border">
//             Email Address: ${detail.email}
//         </div>
//         <div class="nameBox black-border">
//             Gender: ${detail.gender}
//         </div>
//         <label>Height:
//             <input type="number" class="nameBox black-border" name="height">
//         </label>
//         <label>Weight:
//             <input type="number" class="nameBox black-border" name="weight">
//         </label>
// </div>
// ${detail.favourites ?
// `<div class="nameBox black-border container">Favourites: ${detail.favourites}</div>
// `:
// `
// <div class="nameBox black-border container">
//     Favourites:
// </div>
// `
// }
// ${
// detail.total_calories_burned ?
// `
// <div class="nameBox black-border container">
//     Total Calories burned: ${detail.total_calories_burned}
// </div>
// `

// :`
// <div class="nameBox black-border container">
//     Total Calories burned: ${detail.total_calories_burned}
// </div>
// `
// }

// <input type="submit" value="submit">
// </form>
// `