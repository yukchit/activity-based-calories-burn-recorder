// // AJAX by wung

async function loadActivityRecord() {


    const res = await fetch("/activityRecord");
    const result = await res.json();

    // const duration = (record.ending_time - record.starting_time) * 10
    // const durationFormatMIn = (duration / 1000 / 60) % 60
    // console.log(durationFormatMIn)
    // const durationFormatHour = Math.floor(duration / 1000 / 60 / 60)
    // console.log(durationFormatHour)

    const activityContainer = document.getElementById('outside-recent-activities')
    activityContainer.innerHTML = "";
    for (let record of result) {
        const duration = (Number(new Date(record.ending_time)) - Number(new Date(record.starting_time)));
        const durationFormatMIn = Math.floor((duration / 1000 / 60) % 60);
        const durationFormatHour = Math.floor(duration / 1000 / 60 / 60);
        const durationS = Math.floor((duration / 1000) % 60);
        activityContainer.innerHTML += `
        <div class="alert alert-danger d-none" role="alert" id="delete-success">
        DELETE SUCCESS
        </div>
        <div class="card text-dark alert-success mb-3 container">
        <div class="card-header h4"><b>${record.activity}</b></div>
        <div class="card-body">
            <div class="row">
                <div class="card-title col-sm-7 h5">INTENSITY: ${record.activity_intensity}</div>
                <div class="card-title col-sm-4 h5">CALORIES BURNED: ${record.calories_burned} kcal</div>
            </div>
            <div class="row">
                <div class="card-title col-sm-8">ACTIVITY NAME: ${record.activity_name}</div>
            </div>
            <div class="row">
                <div class="col-sm-7 starting-time">STARTING TIME: ${new Date(record.starting_time)}</div>
                <div class="col-sm-4">DURATION: ${durationFormatHour}hr ${durationFormatMIn}min ${durationS}sec</div>
            </div>
            <div class="row">
                ${
            record.distance ? `
                    <div class="col-sm-4 ">Distance Travelled: ${record.distance} km</div>
                    `: ""
            }
                ${
            record.speed ? `
                    <div class="col-sm-4 ">Speed: ${record.speed} km/hr</div>
                    `: ""
            }
            </div>
            <div class="delete-button">
                <button type="submit" class="btn btn-default btn-sm btn-danger delete-button" data-id="${record.id}">
                    <span class="glyphicon glyphicon-trash"></span>DELETE
                </button>
            </div>
        </div>
    </div>
        </br>`
    }
    if (activityContainer.innerHTML == "") {
        activityContainer.innerHTML += `        <div class="jumbotron container">
            <h1 class="display-4">You have no any record yet</h1>
            <p class="lead"></p>
            <hr class="my-4">
            <p class="h5 lead">Start Your Work Out Now!</p>
            <p class="lead">
              <a class="btn btn-primary btn-lg" href="/StartRecord.html" role="button">Start Your Work Out</a>
            </p>
          </div>`
        return
    }

    activityContainer.onclick = async function (event) {

        if (event.target.matches('.delete-button')) {
            const recordID = event.target.getAttribute('data-id');
            console.log(recordID)
            const record = await fetch(`activityRecord/${recordID}`, {
                method: "DELETE"
            });
            console.log(record)
            const recordInfo = await record.json();
            await loadActivityRecord();
            document.getElementById('delete-success').classList.remove("d-none")
        }
    }

}
    


let x = 0
if (x == 0) {
    loadActivityRecord();
    x += 1;
    console.log(x);
    loadActivityRecord();
}









