// ts by CHiT

export interface User{
    id: number
    name: string
    password: string
    age: number
    gender: string
    date_of_birth: Date
    email: string
    height: number
    weight: number
}

// export interface User{
//     id: number
//     username: string
//     password: string
// }
