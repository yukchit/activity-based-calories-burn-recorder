// ts by wung

import express, { Request, Response, NextFunction } from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import multer from 'multer';
import path from 'path';
//need to connect to database
import pg from 'pg';
import dotenv from 'dotenv';
dotenv.config();
// import grant from 'grant-express';
const grant = require('grant-express');


export const client = new pg.Client({
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: "localhost",
    port: 5432 // always this one
})

client.connect();




const app = express();

app.use(expressSession({
    secret: 'Tecky Cohort9 1st Project Group3',
    resave: true,
    saveUninitialized: true
}));


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// ts by CHiT
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}-${Math.random().toString(36).substring(2)}.${file.mimetype.split('/')[1]}`);
    }
})

export const upload = multer({ storage });


app.use(grant({
    "defaults":{
        "protocol": "http",
        // "host": "localhost:8080",
        "host": process.env.GOOGLE_CLIENT_HOST || "",
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
      },
}));



app.get('/', function (req: Request, res: Response, next: NextFunction) {
    try {
        if (req.session) {
        if (req.session.counter) {
            req.session.counter += 1;
        } else {
            req.session.counter = 1;
        }
        console.log(`Counter is ${req.session.counter}`)
    }
    next();
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[MAIN001] internal server error' });
    }
    
});


app.get('/', function (req, res, next) {
    console.log(`[${new Date().toISOString()}] Request ${req.path}`);
    next();
});

import { userRoutes } from './userRoutes';
import { isLoggedIn } from './guards';
import { recordRoutes } from './recordRoutes';
import { appRecordRoutes } from './appRecordRoutes';
import { activityRecordRoutes } from './activitiesRoutes';
import { profileRoutes } from './profileRoutes';
import { logger } from './logger';
app.use('/', userRoutes);
app.use('/', recordRoutes);
app.use('/', appRecordRoutes);
app.use('/', activityRecordRoutes)
app.use('/', profileRoutes)







app.use(express.static(path.join(__dirname, 'public')));
app.use(isLoggedIn, express.static('protected'));

app.use(function (req: Request, res: Response) {
    res.sendFile(path.join(__dirname, "public/404.html"));
});

app.listen(8080, function () {
    console.log("Listening at http://localhost8080/")
});

