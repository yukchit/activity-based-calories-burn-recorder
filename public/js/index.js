// AJAX by CHiT

async function loadSession(){
    const res = await fetch('/loggedInMainPage');

    if (res.status === 200) {
        const result = await res.json();

        document.getElementById('inputEmail').classList.add('d-none');
        document.getElementById('inputPassword').classList.add('d-none');
        document.getElementById('sign-in-button').classList.add('d-none');
        document.getElementById('sign-in-with-google-button').classList.add('d-none');
        document.getElementById('sign-in-box-question-acc').classList.add('d-none');
        document.getElementById('create-acc-button').classList.add('d-none');

        const signInBox = document.getElementById('sign-in-from');
        signInBox.innerHTML += `
        </br>
        <div class="col-sm-12" id="logged-in-msg">You have already logged in as&nbsp;<b>${result.name}</b>.</div>
        <a href="StartRecord.html" id="start-workout-button"
                            class="btn btn-success btn-block">START YOUR WORKOUT NOW!</a>
        `
    } else {

    }
   
}

loadSession();



// js by CHiT

const searchParams = new URLSearchParams(window.location.search);

const errMessage = searchParams.get('error');

if(errMessage){
    const alertBox = document.createElement('div');
    alertBox.classList.add('alert','alert-danger');
    alertBox.textContent = errMessage;
    document.querySelector('#error-message').appendChild(alertBox);
}

const accSuccessMessage = searchParams.get('accSuccess');

if(accSuccessMessage){
    const alertBox = document.createElement('div');
    alertBox.classList.add('alert','alert-success');
    alertBox.textContent = accSuccessMessage;
    document.querySelector('#accSuccess-message').appendChild(alertBox);
}



function calsBurn() {

    function find(id){
        return document.getElementById(id);
    }

    var age = find("age").value;
    var height = find("height").value;
    var weight = find("weight").value;
    var result = 0;
    if (find("male").checked){
        result = (10 * weight) + (6.25 * height) - (5 * age) + 5;
    } else if (find("female").checked){
        result = (10 * weight) + (6.25 * height) - (5 * age) - 161;
    }
  
    var bmr = result;
    var duration = find("duration").value / 60;

    var walkingGeneralResult = 0;
    walkingGeneralResult = (bmr / 24) * 3 * duration;
    find("walkingGeneralCals").innerHTML = Math.round( walkingGeneralResult );

    var walkingModerateResult = 0;
    walkingModerateResult = (bmr / 24) * 3.3 * duration;
    find("walkingModerateCals").innerHTML = Math.round( walkingModerateResult );

    var walkingVeryBriskPaceResult = 0;
    walkingVeryBriskPaceResult = (bmr / 24) * 5 * duration;
    find("walkingVeryBriskPaceCals").innerHTML = Math.round( walkingVeryBriskPaceResult );

    var runningGeneralResult = 0;
    runningGeneralResult = (bmr / 24) * 7 * duration;
    find("runningGeneralCals").innerHTML = Math.round( runningGeneralResult );

    var running8Result = 0;
    running8Result = (bmr / 24) * 8 * duration;
    find("running8Cals").innerHTML = Math.round( running8Result );

    var running837Result = 0;
    running837Result = (bmr / 24) * 9 * duration;
    find("running837Cals").innerHTML = Math.round( running837Result );

    var running966Result = 0;
    running966Result = (bmr / 24) * 10 * duration;
    find("running966Cals").innerHTML = Math.round( running966Result );

    var running1078Result = 0;
    running1078Result = (bmr / 24) * 11 * duration;
    find("running1078Cals").innerHTML = Math.round( running1078Result );

    var running1126Result = 0;
    running1126Result = (bmr / 24) * 11.5 * duration;
    find("running1126Cals").innerHTML = Math.round( running1126Result );

    var running12Result = 0;
    running12Result = (bmr / 24) * 12.5 * duration;
    find("running12Cals").innerHTML = Math.round( running12Result );

    var running1285Result = 0;
    running1285Result = (bmr / 24) * 13.5 * duration;
    find("running1285Cals").innerHTML = Math.round( running1285Result );

    var running1384Result = 0;
    running1384Result = (bmr / 24) * 14 * duration;
    find("running1384Cals").innerHTML = Math.round( running1384Result );

    var running1448Result = 0;
    running1448Result = (bmr / 24) * 15 * duration;
    find("running1448Cals").innerHTML = Math.round( running1448Result );

    var running1609Result = 0;
    running1609Result = (bmr / 24) * 16 * duration;
    find("running1609Cals").innerHTML = Math.round( running1609Result );

    var running1754Result = 0;
    running1754Result = (bmr / 24) * 18 * duration;
    find("running1754Cals").innerHTML = Math.round( running1754Result );

    var hikingResult = 0;
    hikingResult = (bmr / 24) * 6 * duration;
    find("hikingCals").innerHTML = Math.round( hikingResult );

    var cyclingLeisureResult = 0;
    cyclingLeisureResult = (bmr / 24) * 4 * duration;
    find("cyclingLeisureCals").innerHTML = Math.round( cyclingLeisureResult );

    var cycling1915Result = 0;
    cycling1915Result = (bmr / 24) * 6 * duration;
    find("cycling1915Cals").innerHTML = Math.round( cycling1915Result );

    var cyclingGeneralResult = 0;
    cyclingGeneralResult = (bmr / 24) * 8 * duration;
    find("cyclingGeneralCals").innerHTML = Math.round( cyclingGeneralResult );
    
    var cycling2559Result = 0;
    cycling2559Result = (bmr / 24) * 10 * duration;
    find("cycling2559Cals").innerHTML = Math.round( cycling2559Result );

    var cycling3058Result = 0;
    cycling3058Result = (bmr / 24) * 12 * duration;
    find("cycling3058Cals").innerHTML = Math.round( cycling3058Result );

    var cycling3219Result = 0;
    cycling3219Result = (bmr / 24) * 16 * duration;
    find("cycling3219Cals").innerHTML = Math.round( cycling3219Result );

    var aerobicResult = 0;
    aerobicResult = (bmr / 24) * 6.5 * duration;
    find("aerobicCals").innerHTML = Math.round( aerobicResult );

    var badmintonGeneralResult = 0;
    badmintonGeneralResult = (bmr / 24) * 4.5 * duration;
    find("badmintonGeneralCals").innerHTML = Math.round( badmintonGeneralResult );

    var badmintonCompetitiveResult = 0;
    badmintonCompetitiveResult = (bmr / 24) * 7 * duration;
    find("badmintonCompetitiveCals").innerHTML = Math.round( badmintonCompetitiveResult );

    var basketballGeneralResult = 0;
    basketballGeneralResult = (bmr / 24) * 6 * duration;
    find("basketballGeneralCals").innerHTML = Math.round( basketballGeneralResult );

    var basketballGameResult = 0;
    basketballGameResult = (bmr / 24) * 8 * duration;
    find("basketballGameCals").innerHTML = Math.round( basketballGameResult );

    var bowlingResult = 0;
    bowlingResult = (bmr / 24) * 3 * duration;
    find("bowlingCals").innerHTML = Math.round( bowlingResult );

    var dartsResult = 0;
    dartsResult = (bmr / 24) * 2.5 * duration;
    find("dartsCals").innerHTML = Math.round( dartsResult );

    var golfResult = 0;
    golfResult = (bmr / 24) * 4.5 * duration;
    find("golfCals").innerHTML = Math.round( golfResult );

    var rugbyResult = 0;
    rugbyResult = (bmr / 24) * 10 * duration;
    find("rugbyCals").innerHTML = Math.round( rugbyResult );

    var soccerGeneralResult = 0;
    soccerGeneralResult = (bmr / 24) * 7 * duration;
    find("soccerGeneralCals").innerHTML = Math.round( soccerGeneralResult );

    var soccerCompetitiveResult = 0;
    soccerCompetitiveResult = (bmr / 24) * 10 * duration;
    find("soccerCompetitiveCals").innerHTML = Math.round( soccerCompetitiveResult );

    var squashResult = 0;
    squashResult = (bmr / 24) * 10 * duration;
    find("squashCals").innerHTML = Math.round( squashResult );

    var tableTennisResult = 0;
    tableTennisResult = (bmr / 24) * 4 * duration;
    find("tableTennisCals").innerHTML = Math.round( tableTennisResult );

    var tennisResult = 0;
    tennisResult = (bmr / 24) * 7 * duration;
    find("tennisCals").innerHTML = Math.round( tennisResult );

    var volleyballGeneralResult = 0;
    volleyballGeneralResult = (bmr / 24) * 3 * duration;
    find("volleyballGeneralCals").innerHTML = Math.round( volleyballGeneralResult );

    var volleyballCompetitiveResult = 0;
    volleyballCompetitiveResult = (bmr / 24) * 8 * duration;
    find("volleyballCompetitiveCals").innerHTML = Math.round( volleyballCompetitiveResult );

    var volleyballBeachResult = 0;
    volleyballBeachResult = (bmr / 24) * 8 * duration;
    find("volleyballBeachCals").innerHTML = Math.round( volleyballBeachResult );

  }
  calsBurn()