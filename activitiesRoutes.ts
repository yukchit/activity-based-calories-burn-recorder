// ts by wung

import express, { Request, Response } from 'express';
import { client } from './main';
import { logger } from './logger';


export const activityRecordRoutes = express.Router();

activityRecordRoutes.get('/activityRecord', getUserActivityRecords);

async function getUserActivityRecords(req: Request, res: Response) {
    try {
        const userID = req.session?.user.id;
        // let userID: Number;
        // if (req.session) {
        //     userID = req.session.user.id
        // }

        const result = await client.query(/*sql*/`select * from records where user_id = $1 order by ending_time desc`, [userID])
        res.json(result.rows);
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[ACTI001] internal server error' });
    }

}