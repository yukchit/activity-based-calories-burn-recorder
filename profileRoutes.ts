// ts by Samson

import express, { Request, Response } from 'express';
import { client } from './main';
import { isLoggedInApi } from './guards';

export const profileRoutes = express.Router();

profileRoutes.get('/myProfileDetail', isLoggedInApi, getProfileDetails);

async function getProfileDetails(req: Request, res: Response) {
    try {
        const userID = req.session?.user.id;
        // let userID: number;
        // if (req.session) {
        //     userID = req.session.user.id
        // }

        const result = await client.query(/*sql*/`select profile_pic, name, email, gender, height, weight, favourites, total_calories_burned from users where id = $1`, [userID]);
        // better NOT to select * from database table if there are sensitive information such as password inside the table
        // const result = await client.query(/*sql*/`select * from users where id = $1`, [userID]);
        res.json(result.rows);
    } catch (e) {
        console.error(e.message);
        res.status(500).json({ message: 'internal server error' });
    }

}