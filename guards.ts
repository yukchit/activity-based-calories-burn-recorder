// ts by CHiT

import { Request, Response, NextFunction } from 'express';

export const isLoggedIn = function (req: Request, res: Response, next: NextFunction) {

    if (req.session) {
        if (req.session.user) {
            next();
            return;
        }
    }
    res.redirect('/');
}

export const isLoggedInApi = function (req: Request, res: Response, next: NextFunction) {

    if (req.session) {
        if (req.session.user) {
            next();
            return;
        }
    }
    res.status(401).end();
}