// ts by wung

import express, { Request, Response } from 'express';
import { client } from './main';
import { isLoggedIn } from './guards';
import { User } from './models';
import { logger } from './logger';

export const recordRoutes = express.Router();

recordRoutes.post("/manually-record", isLoggedIn, manuallyRecord);
recordRoutes.delete("/activityRecord/:id", deleteRecord);

export async function manuallyRecord(req: Request, res: Response) {

    try {
        const { activity, activityName, startingTime, DurationHr, DurationMin, DurationSec, distanceTravelled, speedCalculated } = req.body;
        
        const userID = req.session?.user.id;
        // let userID: Number;
        // if (req.session) {
        //     userID = req.session.user.id
        // }

        console.log(DurationHr);
        console.log(DurationMin);
        console.log(DurationSec);
        const endingTime = (Number(new Date(startingTime))) + (DurationHr * 60 * 60 * 1000) + (DurationMin * 60 * 1000) + (DurationSec * 1000);




        const user: User = (await client.query(/* sql */`select date_of_birth, height, weight, gender, id, total_calories_burned from users where id = $1`, [userID])).rows[0];
        // const user = users.rows[0]
        // better NOT to select * from database table if there are sensitive information such as password inside the table

        const start = new Date(startingTime);
        const userBirth = user.date_of_birth;
        const userAge = Number(start) - Number(userBirth);

        const Age = Math.floor(userAge / 60 / 60 / 24 / 365 / 1000);
        const height = user.height; // parseFloat, parseInt
        const weight = user.weight;
        const gender = user.gender;
        let result = 0;

        if (gender === "Male") {
            result = (10 * weight) + (6.25 * height) - (5 * Age) + 5;
        } else if (gender == "Female") {
            result = (10 * weight) + (6.25 * height) - (5 * Age) - 161;
        }

        //    console.log(`${height}, ${weight}, ${result}, ${gender}`)
        let bmr = result;
        let countDuration = endingTime - Number(new Date(startingTime));
        let duration = (countDuration / 60 / 60 / 1000);
        let intensity: string;

        let userActivity: string;
        let finalResult = 0;

        if (activity == "walk") {
            let walkingGeneralResult = 0;
            walkingGeneralResult = (bmr / 24) * 3 * duration;
            finalResult = walkingGeneralResult;
            intensity = "General";
            userActivity = "Walking";
        }
        else if (activity == "walkingModerate") {
            let walkingGeneralResult = 0;
            walkingGeneralResult = (bmr / 24) * 3.3 * duration;
            finalResult = walkingGeneralResult;
            intensity = "Moderate";
            userActivity = "Walking";
        }
        else if (activity == "walkingVeryBriskPace") {
            let walkingGeneralResult = 0;
            walkingGeneralResult = (bmr / 24) * 5 * duration;
            finalResult = walkingGeneralResult;
            intensity = "Very Brisk Pace";
            userActivity = "Walking";
        }
        else if (activity == "runningGeneral") {
            let runningGeneralResult = 0;
            runningGeneralResult = (bmr / 24) * 7 * duration;
            finalResult = runningGeneralResult;
            intensity = "General";
            userActivity = "Running";
        }
        else if (activity == "running8") {
            let running8Result = 0;
            running8Result = (bmr / 24) * 8 * duration;
            finalResult = running8Result;
            intensity = "Speed: < 8.00 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running837") {
            let running837Result = 0;
            running837Result = (bmr / 24) * 9 * duration;
            finalResult = running837Result;
            intensity = "Speed: 8.00 ~ 8.37 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running966") {
            let running966Result = 0;
            running966Result = (bmr / 24) * 10 * duration;
            finalResult = running966Result;
            intensity = "Speed: 8.37 ~ 9.66 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1078") {
            let running1078Result = 0;
            running1078Result = (bmr / 24) * 11 * duration;
            finalResult = running1078Result;
            intensity = "Speed: 9.66 ~ 10.78 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1126") {
            let running1126Result = 0;
            running1126Result = (bmr / 24) * 11 * duration;
            finalResult = running1126Result;
            intensity = "Speed: 10.78 ~ 11.26 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running12") {
            let running12Result = 0;
            running12Result = (bmr / 24) * 12.5 * duration;
            finalResult = running12Result;
            intensity = "Speed: 11.26 ~ 12.00 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1285") {
            let running1285Result = 0;
            running1285Result = (bmr / 24) * 13.5 * duration;
            finalResult = running1285Result;
            intensity = "Speed: 12.00 ~ 12.85 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1384") {
            let running1384Result = 0;
            running1384Result = (bmr / 24) * 14 * duration;
            finalResult = running1384Result;
            intensity = "Speed: 12.85 ~ 13.84 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1448") {
            let running1448Result = 0;
            running1448Result = (bmr / 24) * 15 * duration;
            finalResult = running1448Result;
            intensity = "Speed: 13.84 ~ 14.48 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1609") {
            let running1609Result = 0;
            running1609Result = (bmr / 24) * 16 * duration;
            finalResult = running1609Result;
            intensity = "Speed: 14.48 ~ 16.09 km/hr";
            userActivity = "Running";
        }
        else if (activity == "running1754") {
            let running1754Result = 0;
            running1754Result = (bmr / 24) * 18 * duration;
            finalResult = running1754Result;
            intensity = "Speed: > 16.09 km/hr";
            userActivity = "Running";
        }
        else if (activity == "hiking") {
            let hikingResult = 0;
            hikingResult = (bmr / 24) * 6 * duration;
            finalResult = hikingResult;
            intensity = "General";
            userActivity = "Hiking";
        }
        else if (activity == "cyclingLeisure") {
            let cyclingLeisureResult = 0;
            cyclingLeisureResult = (bmr / 24) * 4 * duration;
            finalResult = cyclingLeisureResult;
            intensity = "Leisure: < 16.09 km/hr";
            userActivity = "Cycling";
        }
        else if (activity == "cycling1915") {
            let cycling1915Result = 0;
            cycling1915Result = (bmr / 24) * 6 * duration;
            finalResult = cycling1915Result;
            intensity = "Leisure: 16.09 ~ 19.15 km/hr";
            userActivity = "Cycling";
        }
        else if (activity == "cyclingGeneral") {
            let cyclingGeneralResult = 0;
            cyclingGeneralResult = (bmr / 24) * 8 * duration;
            finalResult = cyclingGeneralResult;
            intensity = "General: 19.15 ~ 22.37 km/hr";
            userActivity = "Cycling";
        }
        else if (activity == "cycling2559") {
            let cycling2559Result = 0;
            cycling2559Result = (bmr / 24) * 10 * duration;
            finalResult = cycling2559Result;
            intensity = "Speed: 22.37 ~ 25.59 km/hr";
            userActivity = "Cycling";
        }
        else if (activity == "cycling3058") {
            let cycling3058Result = 0;
            cycling3058Result = (bmr / 24) * 12 * duration;
            finalResult = cycling3058Result;
            intensity = "Speed: 22.59 ~ 32.19 km/hr";
            userActivity = "Cycling";
        }
        else if (activity == "cycling3219") {
            let cycling3219Result = 0;
            cycling3219Result = (bmr / 24) * 16 * duration;
            finalResult = cycling3219Result;
            intensity = "Speed: > 32.19 km/hr";
            userActivity = "Cycling";
        }
        else if (activity == "aerobic") {
            let aerobicResult = 0;
            aerobicResult = (bmr / 24) * 6.5 * duration;
            finalResult = aerobicResult;
            intensity = "General";
            userActivity = "Aerobic";
        }
        else if (activity == "badmintonGeneral") {
            let badmintonGeneralResult = 0;
            badmintonGeneralResult = (bmr / 24) * 4.5 * duration;
            finalResult = badmintonGeneralResult;
            intensity = "General";
            userActivity = "Badminton";
        }
        else if (activity == "badmintonCompetitive") {
            let badmintonCompetitiveResult = 0;
            badmintonCompetitiveResult = (bmr / 24) * 7 * duration;
            finalResult = badmintonCompetitiveResult;
            intensity = "Competitive";
            userActivity = "Badminton";
        }
        else if (activity == "basketballGeneral") {
            let basketballGeneralResult = 0;
            basketballGeneralResult = (bmr / 24) * 6 * duration;
            finalResult = basketballGeneralResult;
            intensity = "General";
            userActivity = "Basketball";
        }
        else if (activity == "basketballGame") {
            let basketballGameResult = 0;
            basketballGameResult = (bmr / 24) * 8 * duration;
            finalResult = basketballGameResult;
            intensity = "Competitive";
            userActivity = "Basketball";
        }
        else if (activity == "bowling") {
            let bowlingResult = 0;
            bowlingResult = (bmr / 24) * 3 * duration;
            finalResult = bowlingResult;
            intensity = "General";
            userActivity = "Bowling";
        }
        else if (activity == "darts") {
            let dartsResult = 0;
            dartsResult = (bmr / 24) * 2.5 * duration;
            finalResult = dartsResult;
            intensity = "General";
            userActivity = "Darts";
        }
        else if (activity == "golf") {
            let golfResult = 0;
            golfResult = (bmr / 24) * 4.5 * duration;
            finalResult = golfResult;
            intensity = "General";
            userActivity = "Golf";
        }
        else if (activity == "rugby") {
            let rugbyResult = 0;
            rugbyResult = (bmr / 24) * 10 * duration;
            finalResult = rugbyResult;
            intensity = "General";
            userActivity = "Rugby";
        }
        else if (activity == "soccerGeneral") {
            let soccerGeneralResult = 0;
            soccerGeneralResult = (bmr / 24) * 7 * duration;
            finalResult = soccerGeneralResult;
            intensity = "General";
            userActivity = "Soccer";
        }
        else if (activity == "soccerCompetitive") {
            let soccerCompetitiveResult = 0;
            soccerCompetitiveResult = (bmr / 24) * 10 * duration;
            finalResult = soccerCompetitiveResult;
            intensity = "Competitive";
            userActivity = "Soccer";
        }
        else if (activity == "squash") {
            let squashResult = 0;
            squashResult = (bmr / 24) * 10 * duration;
            finalResult = squashResult;
            intensity = "General";
            userActivity = "Squash";
        }
        else if (activity == "tableTennis") {
            let tableTennisResult = 0;
            tableTennisResult = (bmr / 24) * 4 * duration;
            finalResult = tableTennisResult;
            intensity = "General";
            userActivity = "Table Tennis (Ping Pong)";
        }
        else if (activity == "tennis") {
            let tennisResult = 0;
            tennisResult = (bmr / 24) * 7 * duration;
            finalResult = tennisResult;
            intensity = "General";
            userActivity = "Tennis";
        }
        else if (activity == "volleyballGeneral") {
            let volleyballGeneralResult = 0;
            volleyballGeneralResult = (bmr / 24) * 3 * duration;
            finalResult = volleyballGeneralResult;
            intensity = "General";
            userActivity = "Volleyball";
        }
        else if (activity == "volleyballCompetitive") {
            let volleyballCompetitiveResult = 0;
            volleyballCompetitiveResult = (bmr / 24) * 8 * duration;
            finalResult = volleyballCompetitiveResult;
            intensity = "Competitive";
            userActivity = "Volleyball";
        }
        else if (activity == "volleyballBeach") {
            let volleyballBeachResult = 0;
            volleyballBeachResult = (bmr / 24) * 8 * duration;
            finalResult = volleyballBeachResult;
            intensity = "Beach";
            userActivity = "Volleyball";
        } else {
            finalResult = 0;
            intensity = "Error";
            userActivity = "Error";
        }

        await client.query(/* sql */`INSERT INTO records (activity, activity_name, starting_time, ending_time, user_id, calories_burned, activity_intensity, distance, speed, created_at, updated_at) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, NOW(), NOW()) RETURNING id`
            , [userActivity, activityName, new Date(startingTime), new Date(endingTime), userID, Math.round(finalResult), intensity, distanceTravelled, speedCalculated]);
        await client.query(/*sql*/`UPDATE users SET total_calories_burned = total_calories_burned+${Math.round(finalResult)} where id = $1`, [userID]);

        res.redirect("/Activities.html");
    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[REC001] internal server error' });
    }
}




export async function deleteRecord(req: Request, res: Response) {
    try {
        const recordID = parseInt(req.params.id);

        const userID = req.session?.user.id;
        // let userID: Number;
        // if (req.session) {
        //     userID = req.session.user.id;
        // }


        if (isNaN(recordID)) {
            res.status(400).json({ msg: "id is not a number!" });
        }


        const activity = await client.query(/*sql*/`select * from records where id = $1`, [recordID]);
        const activityCal = activity.rows[0];

        await client.query(/*sql*/`DELETE FROM records where id = $1`, [recordID]);
        await client.query(/*sql*/`UPDATE users SET total_calories_burned =  total_calories_burned - ${activityCal.calories_burned} where id = $1`, [userID]);
        res.json({ success: true });

    } catch (e) {
        logger.info(e.message);
        res.status(500).json({ message: '[REC002] internal server error' });
    }
}