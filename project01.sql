// Tecky Project 01
// Member: CHiT (@yukchit)
// Member: wung ah (@ahwungteen)
// Member: SensitiveTeeth (@SensitiveTeeth)

psql

create database project01;

\c project01;

create table users(
    id serial primary key,
    name varchar(255) not null,
    password varchar(255) not null,
    email varchar(255) not null,
    gender varchar(255) not null,
    height integer not null,
    weight integer not null,
    date_of_birth date not null,
    profile_pic text,
    favourites text,
    total_calories_burned integer,
    created_at timestamp,
    updated_at timestamp
);


create table records(
    id serial,
    user_id integer,
    foreign key (user_id) references users(id),
    activity varchar(255) not null,
    activity_intensity varchar(255),
    activity_name varchar(255),
    starting_time timestamp not null,
    ending_time timestamp not null,
    distance decimal(3,2),
    speed decimal(3,2),
    calories_burned integer,
    created_at timestamp,
    updated_at timestamp
);

